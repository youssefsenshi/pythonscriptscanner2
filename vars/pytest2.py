import requests
import sys
import json
import os
import requests
import base64
from requests.exceptions import ConnectionError




def testVul(harbor_link,library,image,tag,username,password):

	#autentification
	token = base64.b64encode(bytes("%s:%s" % (username,password), 'ascii'))
	auth_header = "Basic %s" % token.decode('ascii')
	headers1 = {'Authorization': auth_header}
	resp = requests.get(f'http://'+harbor_link+'/api/v2.0/users/current',headers=headers1)
	
	
	#filrage du json
	if str(resp)=="<Response [401]>":
		print ('Authentication failed')
	else: 
		#print ('Authentication successful')

		headers = {'X-Accept-Vulnerabilities': 'application/vnd.security.vulnerability.report; version=1.1'}
		response = requests.get('http://'+harbor_link+'/api/v2.0/projects/'+library+'/repositories/'+image+'/artifacts/'+tag+'?with_scan_overview=true', headers=headers)


		try:
		    json.loads(response.text)["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]
		except KeyError as e:
			var_exists = False
			print ("Err: Image not Found")
		except Exception as e:
			print ("Harbor is not working properly")
			
		scan_overview=json.loads(response.text)["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"];
		severity=scan_overview["severity"];
		summary=scan_overview["summary"];
		total=summary["total"];
		fixable=summary["fixable"];
		global Critical
		global	High
		global	Low
		global	Medium	
			
		scan_status=scan_overview["scan_status"];
		#print (scan_status)
		if scan_status == "Success":
			if severity == "None":
				print( 'Great job. Your image is scanned and has no vulnerabilities')
			else:
				print ('Your image is scanned, but it has total', total ,'vulnerabilities.','With only', fixable ,'fixable \n',)
		else:
			print("Image not scanned Yet")
				
				
#catch typing errors
try:
	#for library and image
	for i in range(2, 4):
		valid_string=True
		string = sys.argv[i]
		for char in string: 
			if char.isalpha() or char.isnumeric() or char == '_' or char == '-': 
				continue     
			else:
				valid_string=False
				print("Library or/and image name(s) not valid") 
				exit()
	#for tag
	for char in sys.argv[4]:
		if char.isalpha() or char.isnumeric() or char== '.':
			valid_string=True
			continue
		else: 
			print('Tag not valid')
			exit()


	testVul(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6])
except IndexError as e :
	print("You have an ';' added wwrong somewhere")
except ConnectionError as e :
	print("Connection error, Your url may be unvalid")

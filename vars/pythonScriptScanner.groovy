def call(Map config = [:]) {
    final groovyContent = libraryResource('pytest1.groovy')
    writeFile(file: 'pytest1.groovy', text: groovyContent)
    return "groovy "+ 'pytest1.groovy' +" ${config.DOMAIN} ${config.LIBRARY} ${config.IMAGE} ${config.TAG} ${config.USERNAME} ${config.PASSWORD}"
}

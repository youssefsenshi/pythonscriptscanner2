def call(Map config = [:]) {
  script{
    currentBuild.description ="${config.RESULT}"
    if (config.RESULT == 'Great job. Your image is scanned and has no vulnerabilities') {
      manager.addShortText("No Vulnerabilities", "black", "lightgreen", "0px", "white")

    } else if (config.RESULT.substring(0,40) == 'Your image is scanned, but it has total:') {
      manager.addShortText("Vulnerabilities", "black", "red", "0px", "white")
      error("Build failed because of vulnerabilities found in the image.")

    } else {
      error("Build failed because ${config.RESULT}.")
    }
  }
}

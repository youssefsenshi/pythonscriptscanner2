#! /usr/bin/env groovy

@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7')
import groovyx.net.http.HTTPBuilder;
import groovyx.net.http.RESTClient;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HttpContext;
import org.apache.http.HttpRequest;
import groovy.json.JsonSlurper;
import static javax.swing.JOptionPane.showInputDialog;
@Grab(group='org.apache.httpcomponents', module='httpclient', version='4.5.2')
import groovy.json.*
import org.apache.http.client.methods.*
import org.apache.http.entity.*
import org.apache.http.impl.client.*




def testVulnerability(harbor_link,library,image,tag,username,password){
	
	//aut
	def http = new HTTPBuilder("http://harbor.domain")
	def responseCode
	def credentials = username + ":" + password
	def authString = credentials.getBytes().encodeBase64().toString()
	http.get (
	   path : '/api/v2.0/users/current',
	   headers: [Authorization:'Basic ' + authString ],
	   contentType : "application/json"
	){resp,reader->
	responseCode = "${resp.statusLine}"}
	
	
	//conditions
	if( responseCode == "HTTP/1.1 200 OK") {
		int count=0
		while(count<120 ) {
			def baseUrl = new URL('http://'+harbor_link+'/api/v2.0/projects/'+library+'/repositories/'+image+'/artifacts/'+tag+'?with_scan_overview=true')
			HttpURLConnection connection = (HttpURLConnection) baseUrl.openConnection();
			connection.addRequestProperty("X-Accept-Vulnerabilities", "application/vnd.security.vulnerability.report; version=1.1")
			connection.addRequestProperty("Accept", "application/json")
			connection.with {
				doOutput = true
				requestMethod = 'GET'
				def myresponse = content.text
				def parser = new JsonSlurper()
				def json = parser.parseText(myresponse)
				def scan_status=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["scan_status"]
				def severity=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["severity"]
				

				if(scan_status == "Success"){
					if (severity == "None"){
						println('Great job. Your image is scanned and has no vulnerabilities')
					} else{
					def total=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["summary"]["total"]
					def fixable=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["summary"]["fixable"]
					def critical=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["summary"]["summary"]["Critical"]
					def high=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["summary"]["summary"]["High"]
					def low=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["summary"]["summary"]["Low"]
					def medium=json["scan_overview"]["application/vnd.security.vulnerability.report; version=1.1"]["summary"]["summary"]["Medium"]
					def respp ='Your image is scanned, but it has total: ' + total + ' vulnerabilities.With only ' + fixable + ' fixable \n' + 'Critical:' + critical + '\n' + 'High:' + high + '\n' + 'Low:' + low + '\n' + 'Medium:' + medium 
					println respp
					count=120
					System.exit(0)
					}
				} else if(scan_status == "Running"){
					count++
					int waitFor = 5;
					Thread.sleep(waitFor * 1000)

				} else {
					println 'Scan not performed. \nScan status: ' + scan_status
					System.exit(0)
				}
			}
		}
	} else {
	  println "Login failed"
	  println "${conn.responseCode}: ${conn.responseMessage}"
	  System.exit(0)
	}
}

try {
	testVulnerability(args[0],"library","node","latest","admin","Harbor12345")
} catch(groovyx.net.http.HttpResponseException  e1) {
   println "Unauthorized, your credentials are wrong."
} catch(java.io.FileNotFoundException  e2) {
   println "Image not found."
} catch(java.lang.NullPointerException  e3) {
   println "Image not scanned."
} catch(Exception  ex) {
   println "Your inputs are wrong."
}


